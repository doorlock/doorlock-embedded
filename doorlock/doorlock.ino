#include <SoftwareSerial.h>

SoftwareSerial RFID(2, 3);

String rfid;
String eth;
int relay = 7;
const String rfid_cards = "28014460535E 280163308FF5 280144447E57 280144655F57 28014446163D";


void setup() {
  pinMode(relay, OUTPUT);
  digitalWrite(relay, LOW);
  Serial.begin(9600);
  RFID.begin(9600);
}

void loop() {
  if(canOpenDoorByRfid(loadRfid())){ openDoor(); }
}

String loadRfid() {
  String message = "";
  char c = (char) 0;
  while(RFID.available() > 0) {
    c = RFID.read();
    message += c;
    Serial.println(message);
    if (message.length() == 13){
      return message.substring(1,13);
    }
  }
  return "nothing";
}

boolean canOpenDoorByEth(String eth) {
  return false;
}

boolean canOpenDoorByRfid(String rfid) {
  if(rfid_cards.indexOf(rfid) >= 0){
    return true;
  }
  return false;
}

void openDoor() {
  digitalWrite(relay, HIGH);
  delay(100);
  digitalWrite(relay, LOW);
}


